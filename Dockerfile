FROM yiisoftware/yii2-php:8.0-apache
LABEL authors="Bksi"

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    cron \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd ctype simplexml

# Composer uses super user by default
ENV COMPOSER_ALLOW_SUPERUSER 1

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer



# Set working directory
WORKDIR /var/www/backend

# Copy existing application directory contents
COPY . .

RUN ls -lah && composer install --prefer-dist --no-scripts --no-dev --no-autoloader --no-interaction && \
    composer dump-autoload --optimize

# Change document root for Apache
RUN sed -i -e 's|/app/web|/var/www/backend/public|g' /etc/apache2/sites-available/000-default.conf

COPY ./crontab/send-sms /etc/cron.d/crontab

RUN chmod 0644 /etc/cron.d/crontab

RUN touch /var/log/sms_app.log
RUN chmod 644 /var/log/sms_app.log

#install crontab
RUN /usr/bin/crontab /etc/cron.d/crontab

# run cron service
CMD cron -f