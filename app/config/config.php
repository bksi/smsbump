<?php

return [
    'aliases' => [
        '@app' => __DIR__.'/..',
    ],
    'components' => [
        'db' => [
            "params" => [
                "class" => "app\\core\\db\\Connection",
            ],
            'config' => [
                'dsn' => 'mysql:host=db;dbname=smsbump',
                'username' => 'root',
                'password' => 'r0otPr@sswoRth',
            ],
        ],
        'log' => [
            'params' => [
                "class" => "app\\components\\LogFactory",
            ],
            'config' => [
                'targetClass' => 'app\\components\\FileTarget',
                'logPath' => 'logs/',
                'logFileName' => 'app_log.txt',
            ]
        ],
    ]
];
